package com.andrei.app;
/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andrei.app.activities.MainActivity;
import com.andrei.app.model.entities.EmergencyModel;
import com.andrei.app.model.entities.NurseModel;
import com.andrei.app.model.request.HandleEmergencyRequestModel;
import com.andrei.app.model.request.NurseResponseForEmergenciesModel;
import com.andrei.app.task.HandleEmergencyTask;

public class EmergenciesListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private EmergenciesRepository _repository;
    private Activity _activity;

    public EmergenciesListAdapter(Activity activity) {
        _context = activity.getApplicationContext();
        _activity = activity;
        _repository = EmergenciesRepository.Instance();
    }

    public void sort(int direction, int rule){
        _repository.setSortOrder(rule);
    }

    @Override
    public int getGroupCount() {
        return _repository.getTotalNumber();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return _repository.getModel(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return _repository.getModel(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return ((EmergencyModel) (getGroup(groupPosition))).getEmergencyId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getGroupId(groupPosition);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.list_item_emergency_header, null);

        LinearLayout bloodPressureAlert = (LinearLayout)convertView.findViewById(R.id.llBloodPressureLimit);
        LinearLayout oxygenSaturationAler = (LinearLayout)convertView.findViewById(R.id.llOxygenSaturationLimit);
        LinearLayout temperatureAlert = (LinearLayout)convertView.findViewById(R.id.llTemperatureLimit);
        LinearLayout heartRateAlert = (LinearLayout)convertView.findViewById(R.id.llHeartRateLimit);

        TextView tvFullPatientName = (TextView) convertView.findViewById(R.id.tvPatientFullName);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);

        ImageView tvHeartRateImage_ = (ImageView) convertView.findViewById(R.id.tvHeartRateImage_);
        ImageView tvBloodPressureImage_ = (ImageView) convertView.findViewById(R.id.tvBloodPressureImage_);
        ImageView tvOxygenSaturationImage_ = (ImageView) convertView.findViewById(R.id.tvOxygenImage_);
        ImageView tvTemperatureImage_ = (ImageView) convertView.findViewById(R.id.bodyTemperatureImage_);


        final EmergencyModel model = (EmergencyModel) getGroup(groupPosition);

        if(model.getHeartRate()==-1)
            tvHeartRateImage_.setVisibility(View.INVISIBLE);
        if(model.getOxygenSaturation()==-1)
            tvOxygenSaturationImage_.setVisibility(View.INVISIBLE);
        if(model.getBloodPressure()==-1)
            tvBloodPressureImage_.setVisibility(View.INVISIBLE);
        if(model.getTemperature()==-1)
            tvTemperatureImage_.setVisibility(View.INVISIBLE);

        if(model.getBloodPressureAlarm()){
            bloodPressureAlert.setBackgroundColor(Color.RED);
        }
        if(model.getHeartRateAlarm()){
            heartRateAlert.setBackgroundColor(Color.BLACK);
        }
        if(model.getOxygenSaturationAlarm()){
            oxygenSaturationAler.setBackgroundColor(Color.RED);
        }
        if(model.getTemperatureAlarm()){
            temperatureAlert.setBackgroundColor(Color.RED);
        }

        tvFullPatientName.setText(model.getName());
        String description = model.getPatientRequest();
        if (description.length() < 15) {
            tvDescription.setText(description);
        } else {
            tvDescription.setText(description.substring(0, 15) + "...");
        }


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.list_item_emergency_body, null);
        TextView tvFloor = (TextView) convertView.findViewById(R.id.tvFloor);
        TextView tvRoom = (TextView) convertView.findViewById(R.id.tvRoom);
        TextView tvBed = (TextView) convertView.findViewById(R.id.tvBed);
        TextView tvFullDescription = (TextView) convertView.findViewById(R.id.tvFullDescription);
       // TextView tvShortDescription = (TextView) convertView.findViewById(R.id.tvDescription);

        final EditText etNurseMessage=(EditText) convertView.findViewById(R.id.etNurseMessage);
        TextView tvBloodPressure = (TextView) convertView.findViewById(R.id.tvBloodPressure);
        TextView tvBloodPressureText = (TextView) convertView.findViewById(R.id.tvBloodPressureText);
        ImageView tvBloodPressureImage = (ImageView) convertView.findViewById(R.id.tvBloodPressureImage);

        TextView tvHeartRate = (TextView) convertView.findViewById(R.id.tvHeartRate);
        TextView tvHeartRateText = (TextView) convertView.findViewById(R.id.tvHeartRateText);
        ImageView tvHeartRateImage = (ImageView) convertView.findViewById(R.id.tvHeartRateImage);

        TextView tvOxygenSaturation = (TextView) convertView.findViewById(R.id.tvOxygen);
        TextView tvOxygenSaturationText = (TextView) convertView.findViewById(R.id.tvOxygenText);
        ImageView tvOxygenSaturationImage = (ImageView) convertView.findViewById(R.id.tvOxygenImage);

        TextView tvTemperature = (TextView) convertView.findViewById(R.id.tvTemperature);
        TextView tvTemperatureText = (TextView) convertView.findViewById(R.id.bodyTemperature2);
        ImageView tvTemperatureImage = (ImageView) convertView.findViewById(R.id.bodyTemperature1);



        ImageButton handleYes = (ImageButton) convertView.findViewById(R.id.handleYes);
        ImageButton handleNo = (ImageButton) convertView.findViewById(R.id.handleNo);

        final EmergencyModel model = (EmergencyModel) getChild(groupPosition, childPosition);

        tvFloor.setText("" + model.getFloor());
        tvRoom.setText("" + model.getRoom());
        tvBed.setText("" + model.getBed());
        tvFullDescription.setText(model.getPatientRequest());
        //tvShortDescription.setText(model.getPatientRequest());

        if(model.getBloodPressure()!=-1) {
            tvBloodPressure.setText("" + model.getBloodPressure());
        }else{
            tvBloodPressure.setVisibility(View.GONE);
            tvBloodPressureText.setVisibility(View.GONE);
            tvBloodPressureImage.setVisibility(View.GONE);
        }
        if(model.getTemperature()!=-1) {
            tvTemperature.setText("" + model.getTemperature());
        }else{
            tvTemperature.setVisibility(View.GONE);
            tvTemperatureText.setVisibility(View.GONE);
            tvTemperatureImage.setVisibility(View.GONE);
        }
        if(model.getHeartRate()!=-1) {
            tvHeartRate.setText("" + model.getHeartRate());
        }else{
            tvHeartRate.setVisibility(View.GONE);
            tvHeartRateText.setVisibility(View.GONE);
            tvHeartRateImage.setVisibility(View.GONE);
        }
        if(model.getOxygenSaturation()!=-1) {
            tvOxygenSaturation.setText("" + model.getOxygenSaturation());
        }else{
            tvOxygenSaturation.setVisibility(View.GONE);
            tvOxygenSaturationText.setVisibility(View.GONE);
            tvOxygenSaturationImage.setVisibility(View.GONE);
        }

        handleYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalSettings settings = new LocalSettings(_context);
                NurseModel nurseModel = settings.getSettings();

                HandleEmergencyRequestModel handleEmergencyRequestModel = new HandleEmergencyRequestModel(nurseModel.getId(),model.getEmergencyId(),true,nurseModel.getSessionId(),etNurseMessage.getText().toString());

                (new HandleEmergencyTask(handleEmergencyRequestModel)).response();
                _repository.remove(model);
                notifyDataSetChanged();
            }
        });
        handleNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocalSettings settings = new LocalSettings(_context);
                NurseModel nurseModel = settings.getSettings();

                HandleEmergencyRequestModel handleEmergencyRequestModel = new HandleEmergencyRequestModel(nurseModel.getId(),model.getEmergencyId(),false,nurseModel.getSessionId(),etNurseMessage.getText().toString());

                (new HandleEmergencyTask(handleEmergencyRequestModel)).response();
                _repository.remove(model);
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
