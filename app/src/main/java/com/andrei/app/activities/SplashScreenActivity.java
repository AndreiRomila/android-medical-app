package com.andrei.app.activities;

/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.andrei.app.R;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                com.andrei.app.LocalSettings settings = new com.andrei.app.LocalSettings(getApplicationContext());
                if(settings.getSettings() != null){
                    intent = new Intent(getApplicationContext(),MainActivity.class);
                }else{
                    intent = new Intent(getApplicationContext(),LoginActivity.class);
                }
                startActivity(intent);
                finish();
            }
        },1000);
    }
}
