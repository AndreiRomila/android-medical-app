package com.andrei.app.activities;

/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;




import com.andrei.app.R;
import com.andrei.app.ServerConnection;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.LoginRequestModel;
import com.andrei.app.task.LoginTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class LoginActivity extends Activity implements View.OnClickListener {
    private EditText _usernameEditText;
    private EditText _passwordEditText;
    private Button _buttonSubmit;
    private ImageButton _buttonSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        _usernameEditText = (EditText)findViewById(R.id.etUsername);
        _passwordEditText = (EditText)findViewById(R.id.etPassword);
        _buttonSettings=(ImageButton)findViewById(R.id.btnSettings);
        _buttonSettings.setOnClickListener(this);
        _buttonSubmit = (Button)findViewById(R.id.btnSubmit);
        _buttonSubmit.setOnClickListener(this);
        _buttonSubmit.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit: {
                doLogin();
                break;
            }
            case R.id.btnSettings: {
                ChangeIp();
                break;
            }
        }
    }
    private void ChangeIp(){
        Intent intent=new Intent(this,ChangeIpActivity.class);
        startActivity(intent);
        finish();
    }
    private void doLogin(){
        Socket socket;
        String username = _usernameEditText.getText().toString();
        String password = _passwordEditText.getText().toString();
        if(username==null||username.isEmpty()){
            Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password==null||password.isEmpty()){
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
            return;
        }

        String string=null;
        try{
            BufferedReader in=new BufferedReader(new InputStreamReader(openFileInput("ip.txt")));
            string=in.readLine();
            ServerInterface.SERVER_IP=string;
        }catch(Exception e){

        }
        LoginRequestModel loginModel = new LoginRequestModel(username,password);
        new LoginTask(this,loginModel).execute();
        _buttonSubmit.setEnabled(false);
    }
    public void redirectToMainScreen(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void showErrorMessage(String error){
        _buttonSubmit.setEnabled(true);
        new AlertDialog.Builder(this)
                .setMessage(error)
                .setTitle("Login Error")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create()
                .show();
    }
}
