package com.andrei.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;

import com.andrei.app.R;
import com.andrei.app.ServerInterface;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Andrei on 8/6/2015.
 */
public class ChangeIpActivity extends Activity implements View.OnClickListener{
    private EditText _ipValueEditText;
    private Button _buttonCancel;
    private Button _buttonChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_ip);
        _ipValueEditText=(EditText)findViewById(R.id.ip_value);
        _buttonCancel=(Button)findViewById(R.id.btnCancel);
        _buttonCancel.setOnClickListener(this);
        _buttonChange=(Button)findViewById(R.id.btnChange);
        _buttonChange.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancel: {
                Back_to_Login();
                break;
            }
            case R.id.btnChange: {
                String ip=_ipValueEditText.getText().toString();
                if((ip==null) || ip.isEmpty()) {
                    Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show();
                    return;
                }
                File file = new File(getFilesDir(), "ip.txt");
                FileOutputStream outputStream;
                try {
                    outputStream = openFileOutput("ip.txt",MODE_PRIVATE);
                    outputStream.write(ip.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ServerInterface.SERVER_IP=ip;
                Back_to_Login();
                break;
            }
        }

    }
    private void Back_to_Login(){
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
