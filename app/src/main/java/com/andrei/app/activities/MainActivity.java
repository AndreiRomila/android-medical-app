package com.andrei.app.activities;

/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.andrei.app.EmergenciesListAdapter;
import com.andrei.app.EmergenciesRepository;
import com.andrei.app.LocalSettings;
import com.andrei.app.R;
import com.andrei.app.ServerConnection;
import com.andrei.app.model.request.EmergenciesRequestModel;
import com.andrei.app.model.request.LogoutRequestModel;
import com.andrei.app.model.entities.NurseModel;
import com.andrei.app.task.GetAllServerMessagesTask;
import com.andrei.app.task.GetEmergencyTask;
import com.andrei.app.task.LogoutTask;
import java.io.IOException;


public class MainActivity extends Activity implements View.OnClickListener{
    private NurseModel _nurseModel;
    private LocalSettings _settings;
    private TextView _tvFullName;
    private EmergenciesListAdapter _listAdapter;
    private ExpandableListView _emergenciesListView;
    private ImageButton _buttonSignOut;
    private ImageButton _buttonSettings;
    private ExpandableListView _elvEmergencies;
    private ImageButton _buttonSortTemperature;
    private ImageButton _buttonSortHeartRate;
    private ImageButton _buttonSortOxygen;
    private ImageButton _buttonSortBloodPresure;
    private ImageButton _buttonSortPriority;
    private EmergenciesRequestModel model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _settings = new LocalSettings(getApplicationContext());
        _nurseModel = _settings.getSettings();
        model= new EmergenciesRequestModel(_nurseModel.getDepartmentId(),_nurseModel.getId(),_nurseModel.getSessionId());
        new GetEmergencyTask(this,model).execute();

        _tvFullName = (TextView)findViewById(R.id.tvFullName);
        _tvFullName.setText(_nurseModel.getLastname() + " " + _nurseModel.getFirstname());
        _emergenciesListView = (ExpandableListView)findViewById(R.id.elvEmergencies);
        _listAdapter = new EmergenciesListAdapter(this);
        _emergenciesListView.setAdapter(_listAdapter);

        _buttonSettings = (ImageButton)findViewById(R.id.btnSettings);
        _buttonSignOut = (ImageButton)findViewById(R.id.btnSignout);

        _elvEmergencies=(ExpandableListView)findViewById(R.id.elvEmergencies);

       // _buttonSortHeartRate = (ImageButton)findViewById(R.id.btnSortHeartRate);
        //_buttonSortTemperature = (ImageButton)findViewById(R.id.btnSortTemperature);
       // _buttonSortOxygen = (ImageButton)findViewById(R.id.btnSortOxygenSaturation);
        //_buttonSortBloodPresure = (ImageButton)findViewById(R.id.btnSortBloodPresure);
        //_buttonSortPriority = (ImageButton)findViewById(R.id.btnSortPriority);

        _buttonSignOut.setOnClickListener(this);
        _buttonSettings.setOnClickListener(this);
        /*_buttonSortHeartRate.setOnClickListener(this);
        _buttonSortPriority.setOnClickListener(this);
        _buttonSortTemperature.setOnClickListener(this);*/
        //_buttonSortBloodPresure.setOnClickListener(this);
       // this.close();
        (new GetAllServerMessagesTask(this,model,true)).execute();
    }
    public void refreshList(){
        _listAdapter.sort(1, EmergenciesRepository.ORDER_PRIORITY);
        _listAdapter.notifyDataSetChanged();
    }
    public void scrollMyListViewToBottom(){
        _elvEmergencies.post(new Runnable() {
            @Override
            public void run() {
                _elvEmergencies.setSelection(_listAdapter.getGroupCount()-1);
            }
        });
    }
    public void close(){
        Intent intent = new Intent(this,LoginActivity.class);
        _settings.deleteSettings();
        startActivity(intent);
        try {
            ServerConnection.disconnect();
        }catch (IOException e){
            return ;
        }
        finish();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSettings:
                showNotification("Settings","setiings");
                break;
            case R.id.btnSignout:
                showNotification("Logout","Logout");
                logout();
                break;
            /*case R.id.btnSortHeartRate:
                _listAdapter.sort(1, EmergenciesRepository.ORDER_HEARTRATE);
                _listAdapter.notifyDataSetChanged();
                break;
            case R.id.btnSortTemperature:
                _listAdapter.sort(1,EmergenciesRepository.ORDER_TEMPERATURE);
                _listAdapter.notifyDataSetChanged();
                break;
            case R.id.btnSortBloodPresure:
                _listAdapter.sort(1,EmergenciesRepository.ORDER_BLOODPRESSURE);
                _listAdapter.notifyDataSetChanged();
                break;
            case R.id.btnSortOxygenSaturation:
                _listAdapter.sort(1,EmergenciesRepository.ORDER_OXYGENSATURATION);
                _listAdapter.notifyDataSetChanged();
                break;
            case R.id.btnSortPriority:
                _listAdapter.sort(1,EmergenciesRepository.ORDER_PRIORITY);
                _listAdapter.notifyDataSetChanged();
                break;*/
        }
    }
    public void showNotification(String title,String message){
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void logout(){
        LogoutRequestModel model = new LogoutRequestModel(_nurseModel.getId(),_nurseModel.getSessionId());
        new LogoutTask(this,model).logout();
    }
}
