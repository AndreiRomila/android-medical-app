package com.andrei.app;
/**
 * Created by Andrei on 7/31/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.andrei.app.model.entities.NurseModel;

import org.json.JSONException;
import org.json.JSONObject;


public class LocalSettings {
    private static String USER_DETAILS = "Settings";
    private static String GCMID = "GcmId";
    private Context _context;

    public LocalSettings(Context context) {
        _context = context;
    }

    public void saveSettings(JSONObject settings) {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(USER_DETAILS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = applicationPreferences.edit();
        try {
            editor.putString(ServerInterface.NURSE_INTERFACE.FIRSTNAME, settings.getString(ServerInterface.NURSE_INTERFACE.FIRSTNAME));
            editor.putString(ServerInterface.NURSE_INTERFACE.LASTNAME, settings.getString(ServerInterface.NURSE_INTERFACE.LASTNAME));
            editor.putString(ServerInterface.NURSE_INTERFACE.USERNAME, settings.getString(ServerInterface.NURSE_INTERFACE.USERNAME));
            editor.putString(ServerInterface.NURSE_INTERFACE.SESSIONID, settings.getString(ServerInterface.NURSE_INTERFACE.SESSIONID));
            editor.putInt(ServerInterface.NURSE_INTERFACE.ID, settings.getInt(ServerInterface.NURSE_INTERFACE.ID));
            editor.putInt(ServerInterface.NURSE_INTERFACE.DEPARTMENTID, settings.getInt(ServerInterface.NURSE_INTERFACE.DEPARTMENTID));
            editor.commit();
        } catch (JSONException e) {
            Log.d("JSON error", e.getMessage());
        }
    }

    public NurseModel getSettings() {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(USER_DETAILS, Context.MODE_PRIVATE);
        try {
            String firstname = applicationPreferences.getString(ServerInterface.NURSE_INTERFACE.FIRSTNAME, null);
            String lastname = applicationPreferences.getString(ServerInterface.NURSE_INTERFACE.LASTNAME, null);
            String username = applicationPreferences.getString(ServerInterface.NURSE_INTERFACE.USERNAME, null);
            String sessionId = applicationPreferences.getString(ServerInterface.NURSE_INTERFACE.SESSIONID, null);
            int id = applicationPreferences.getInt(ServerInterface.NURSE_INTERFACE.ID, 0);
            int departmentId = applicationPreferences.getInt(ServerInterface.NURSE_INTERFACE.DEPARTMENTID, 0);
            if (sessionId == null || sessionId.isEmpty() || username == null || username.isEmpty()) {
                return null;
            }
            return new NurseModel(id, departmentId, firstname, lastname, sessionId, username);

        } catch (Exception e) {
            return null;
        }
    }

    public void deleteSettings() {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(USER_DETAILS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = applicationPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void setGcmId(String gcmId) {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(GCMID, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = applicationPreferences.edit();
        editor.putString(GCMID, gcmId);
        editor.commit();
    }

    public void deleteGcmId() {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(GCMID, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = applicationPreferences.edit();
        editor.clear().commit();
    }

    public String getGcmId() {
        SharedPreferences applicationPreferences = _context.getSharedPreferences(GCMID, Context.MODE_PRIVATE);
        try {
            return applicationPreferences.getString(GCMID, null);
        } catch (Exception e) {
            return null;
        }
    }


}
