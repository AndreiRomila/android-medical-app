package com.andrei.app;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Andrei on 9/14/2015.
 */
public class ServerConnection {

    public static Socket socket;
    public ServerConnection(Socket socket){
        this.socket=socket;
    }
    public static void connect() throws IOException {
        socket = new Socket(ServerInterface.SERVER_IP, ServerInterface.SERVER_PORT);
    }
    public static void disconnect() throws  IOException{
            socket.close();
    }
    public static Socket getSocket(){
        return socket;
    }
}
