package com.andrei.app;
/**
 * Created by Andrei on 7/31/2015.
 */


public class ServerInterface {
    public static String SERVER_IP = "127.0.0.1";
    public static int SERVER_PORT=11000;
    public static String LOGIN_URL = "/api/authentication/login";
    public static String LOGOUT_URL = "/api/authentication/logout";
    public static String EMERGENCIES_URL = "/api/emergencies/getemergencies";
    public static String HANDLE_EMERGENCY_URL = "/api/emergencies/handleemergency";

    public interface PING{
        public static String PACKET_TYPE="packet_type";
        public static String PACKET_TYPE_VALUE="ping";
    }
    public interface LOGIN_INTERFACE {
        public static String PACKET_TYPE="packet_type";
        public static String LOGIN="login";
        public static String USERNAME = "username";
        public static String PASSWORD = "password";
        public static String CONTENT="content";
    }
    public interface LOGOUT_INTERFACE{
        public static String PACKET_TYPE="packet_type";
        public static String CONTENT="content";
        public static String LOGOUT="logout";
        public static String LOGOUT_ACK="logout_ack";
    }

    public interface NURSE_INTERFACE {
        public static String USERNAME = "Username";
        public static String SESSIONID = "SessionId";
        public static String FIRSTNAME = "FirstName";
        public static String LASTNAME = "LastName";
        public static String ID = "Id";
        public static String DEPARTMENTID = "DepartmentId";
    }

    public interface BASE_RESPONSE {
        public static String HASERROR = "HasError";
        public static String ERRORCODE = "error";
        public static String STATUS ="status";
        public static String ERRORMESSAGE = "ErrorMessage";
        public static String DENIED="Password or Username doesn't exist";
    }

    public interface BASE_REQUEST {
        public static String NURSEID = "NurseId";
        public static String SESSIONID = "SessionId";
    }

    public interface EMERGENCIES_REQUEST {
        public static String PACKET_TYPE_VALUE="Emergencies Request";
        public static String PACKET_TYPE="packet_type";
        public static String CONTENT="content";
        public static String DEPARTMENTID = "DepartmentId";
    }

    public interface EMERGENCIES_RESPONSE extends BASE_RESPONSE {
        public static String EMERGENCIES = "Emergencies";
        public static String CONTENT="content";
        public static String PACKET_TYPE="packet_type";
        public static String PACKET_TYPE_VALUE_ON_RECEIVE="msg_ack";
        public static String PACKET_TYPE_VALUE_ON_READ="nurse_notification_nurse_read_ack";
        public static String PACKET_TYPE_VALUE_ON_ACK="nurse_notification_ack";
        public static String PACKET_TYPE_VALUE_ON_DELETE="delete_ack";
        public static String CONFIRMATION="confirmation";
        public static String TEST="test";
        public static String MSG_ID="msg_id";
        public static String DELETE="delete";
    }

    public interface PATIENT {
        public static String PATIENTID = "id";
        public static String PATIENTNAME = "name";
        public static String FLOOR = "floor";
        public static String ROOM = "room";
        public static String BED = "bed";
        public static String BIOMETRICS="biometrics";
    }

    public interface EMERGENCY {
        public static String CONTENT="content";
        public static String EMERGENCYID = "msg_id";
        public static String EMERGENCY_PRIORITY="msg_priority";
        public static String PATIENT="patient";
        public static String TEMPERATURE = "body_temperature";
        public static String BLOODPRESSURE = "blood_presure";
        public static String HEARTRATE = "heart_rate";
        public static String OXYGENSATURATION = "o2";
        public static String PATIENTREQUST = "request";
        public static String VALUE="value";
        public static String ALARM="alarm";
    }
    public interface HANDLE_EMERGENCY extends BASE_REQUEST{
        public static String EMERGENCYID = "EmergencyId";
        public static String CONFIRMATION="confirmation";
        public static String NURSE_MESSAGE="message";
    }

}
