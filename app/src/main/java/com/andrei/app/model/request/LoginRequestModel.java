package com.andrei.app.model.request;

/**
 * Created by Andrei on 7/31/2015.
 */

import android.util.Base64;
import android.util.Log;

import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import com.andrei.app.ServerInterface;
import com.andrei.app.model.base.BaseRequestModel;


public class LoginRequestModel extends BaseRequestModel {
    private String _username;
    private String _password;
    public LoginRequestModel(String username, String password) {
        _username = username;
        _password = password;
        //must encrypt the password

        /*try{
            //step 1 : sha256
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes());
            //step 2 : base64
            _password = Base64.encodeToString(hash, Base64.NO_WRAP);
        } catch (NoSuchAlgorithmException e) {
            // usually shouldn't happens
            Log.d("Encryption", e.getMessage());
            _password = password;
        }*/

    }
    public String getUsername(){
        return _username;
    }
    public String getPassword(){
        return _password;
    }
    @Override
    public HashMap<String,Object> toMap(){
        HashMap<String,Object> map = new HashMap<>();
        HashMap<String,Object> content=new HashMap<>();
        content.put(ServerInterface.LOGIN_INTERFACE.LOGIN,true);
        content.put(ServerInterface.LOGIN_INTERFACE.USERNAME,_username);
        content.put(ServerInterface.LOGIN_INTERFACE.PASSWORD,_password);
        map.put(ServerInterface.LOGIN_INTERFACE.PACKET_TYPE, "login");
        map.put(ServerInterface.LOGIN_INTERFACE.CONTENT,content);
        return map;
    }

}
