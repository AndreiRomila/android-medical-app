package com.andrei.app.model.entities;

/**
 * Created by Andrei on 7/31/2015.
 */

import com.andrei.app.ServerInterface;

import java.util.HashMap;


public class NurseModel {
    private int _id;
    private int _departmentId;
    private String _firstname;
    private String _lastname;
    private String _sessionId;
    private String _username;

    public NurseModel(int id,int departmentId,String firstname,String lastname,String sessionId,String username){
        _id = id;
        _departmentId = departmentId;
        _firstname = firstname;
        _lastname = lastname;
        _sessionId = sessionId;
        _username = username;
    }
    public int getId(){
        return _id;
    }
    public int getDepartmentId(){
        return _departmentId;
    }
    public String getFirstname(){
        return _firstname;
    }
    public String getLastname(){
        return _lastname;
    }
    public String getSessionId(){
        return _sessionId;
    }
    public String getUsername(){
        return _username;
    }
    public HashMap<String,Object> toMap(){
        HashMap<String,Object> map = new HashMap<>();
        map.put(ServerInterface.NURSE_INTERFACE.ID,_id);
        map.put(ServerInterface.NURSE_INTERFACE.DEPARTMENTID,_departmentId);
        map.put(ServerInterface.NURSE_INTERFACE.FIRSTNAME,_firstname);
        map.put(ServerInterface.NURSE_INTERFACE.LASTNAME,_lastname);
        map.put(ServerInterface.NURSE_INTERFACE.SESSIONID,_sessionId);
        map.put(ServerInterface.NURSE_INTERFACE.USERNAME,_username);
        return map;
    }
}
