package com.andrei.app.model.base;

import android.util.Log;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Andrei on 7/31/2015.
 */
public abstract class BaseRequestModel {
    public abstract HashMap<String,Object> toMap();
    public StringEntity toStringEntity(){
        JSONObject jsonObject = new JSONObject(toMap());
        try {
            return new StringEntity(jsonObject.toString());
        }catch (UnsupportedEncodingException ex){
            Log.d("Encoding", ex.getMessage());
            return null;
        }
    }
}
