package com.andrei.app.model.request;

import com.andrei.app.ServerInterface;

import java.util.HashMap;

/**
 * Created by Andrei on 9/23/2015.
 */
public class NurseResponseForEmergenciesModel {
    private int msg_id;
    private String test;
    private boolean confirmation;
    NurseResponseForEmergenciesModel(int msg_id, boolean confirmation, String test){
        this.msg_id=msg_id;
        this.confirmation=confirmation;
        this.test=test;
    }
    public int getMsg_id(){return msg_id;};
    public String getTest(){return test;};
    public boolean getConfirmation(){return confirmation;};
    public HashMap<String,Object> toMap(){
        HashMap<String,Object> map=new HashMap<>();
        HashMap<String,Object> content=new HashMap<>();
        content.put(ServerInterface.EMERGENCIES_RESPONSE.MSG_ID,msg_id);
        content.put(ServerInterface.EMERGENCIES_RESPONSE.CONFIRMATION,confirmation);
        content.put(ServerInterface.EMERGENCIES_RESPONSE.TEST,test);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE,ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE_VALUE_ON_ACK);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.CONTENT,content);
        return map;
    }
}
