package com.andrei.app.model.request;

import com.andrei.app.ServerInterface;

import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Andrei on 9/23/2015.
 */
public class EmergenciesReadAckModel {
    private int msg_id;
    EmergenciesReadAckModel(int msg_id){
        this.msg_id=msg_id;
    }
    public int getMsg_id(){return msg_id;}

    public HashMap<String,Object> toMap(){
        HashMap<String,Object> map=new HashMap<>();
        HashMap<String,Object> content=new HashMap<>();
        content.put(ServerInterface.EMERGENCIES_RESPONSE.MSG_ID,msg_id);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE,ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE_VALUE_ON_READ);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.CONTENT, content);
        return map;
    }
}
