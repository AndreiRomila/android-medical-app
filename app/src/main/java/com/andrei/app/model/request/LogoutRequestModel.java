package com.andrei.app.model.request;

/**
 * Created by Andrei on 7/31/2015.
 */
import com.andrei.app.ServerInterface;
import com.andrei.app.model.base.BaseRequestModel;

import java.net.Socket;
import java.util.HashMap;
import java.util.Objects;


public class LogoutRequestModel extends BaseRequestModel {
    private int _nurseId;
    private String _sessionId;
    public LogoutRequestModel(int nurseId, String sessionId) {
        _nurseId = nurseId;
        _sessionId = sessionId;
    }

    public int getNurseId() {
        return _nurseId;
    }

    public String getSessionId() {
        return _sessionId;
    }

    @Override
    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        HashMap<String,Object> content=new HashMap<>();
        content.put(ServerInterface.LOGOUT_INTERFACE.LOGOUT,true);
        content.put(ServerInterface.BASE_REQUEST.NURSEID, _nurseId);
        content.put(ServerInterface.BASE_REQUEST.SESSIONID, _sessionId);
        map.put(ServerInterface.LOGIN_INTERFACE.PACKET_TYPE,"logout");
        map.put(ServerInterface.LOGOUT_INTERFACE.CONTENT,content);
        return map;
    }
}
