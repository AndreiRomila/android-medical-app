package com.andrei.app.model.entities;

/**
 * Created by Andrei on 7/31/2015.
 */

import android.util.Log;

import com.andrei.app.ServerInterface;

import org.json.JSONObject;


public class EmergencyModel {
    private int _emergencyId;
    private int _emergencyPriority;
    private int _temperature;
    private int _bloodPressure;
    private int _oxygenSaturation;
    private int _heartRate;
    private boolean _temperature_alarm;
    private boolean _bloodPressure_alarm;
    private boolean _oxygenSaturation_alarm;
    private boolean _heartRate_alarm;
    private String _patientRequest;

    private String _patientId;
    private String _patientName;

    private int _floor;
    private int _room;
    private int _bed;

    public EmergencyModel(JSONObject object){
        try{
            JSONObject content=object.getJSONObject(ServerInterface.EMERGENCY.CONTENT);
            _emergencyId = content.getInt(ServerInterface.EMERGENCY.EMERGENCYID);
            _emergencyPriority= content.getInt(ServerInterface.EMERGENCY.EMERGENCY_PRIORITY);

            JSONObject patient=content.getJSONObject(ServerInterface.EMERGENCY.PATIENT);
            _patientId  = patient.getString(ServerInterface.PATIENT.PATIENTID);
            _patientName = patient.getString(ServerInterface.PATIENT.PATIENTNAME);
            _floor = patient.getInt(ServerInterface.PATIENT.FLOOR);
            _room = patient.getInt(ServerInterface.PATIENT.ROOM);
            _bed = patient.getInt(ServerInterface.PATIENT.BED);
            _patientRequest = patient.getJSONObject(ServerInterface.EMERGENCY.PATIENTREQUST).getString(ServerInterface.EMERGENCY.VALUE);

            JSONObject biometrics=patient.getJSONObject(ServerInterface.PATIENT.BIOMETRICS);
            try {
                _temperature = biometrics.getJSONObject(ServerInterface.EMERGENCY.TEMPERATURE).getInt(ServerInterface.EMERGENCY.VALUE);
                _temperature_alarm = biometrics.getJSONObject(ServerInterface.EMERGENCY.TEMPERATURE).getBoolean(ServerInterface.EMERGENCY.ALARM);
            }catch(Exception e){
                _temperature=-1;
                _temperature_alarm=false;
            }
            try {
                _bloodPressure = biometrics.getJSONObject(ServerInterface.EMERGENCY.BLOODPRESSURE).getInt(ServerInterface.EMERGENCY.VALUE);
                _bloodPressure_alarm = biometrics.getJSONObject(ServerInterface.EMERGENCY.BLOODPRESSURE).getBoolean(ServerInterface.EMERGENCY.ALARM);
            }catch(Exception e){
                _bloodPressure=-1;
                _bloodPressure_alarm=false;
            }
            try {
                _oxygenSaturation = biometrics.getJSONObject(ServerInterface.EMERGENCY.OXYGENSATURATION).getInt(ServerInterface.EMERGENCY.VALUE);
                _oxygenSaturation_alarm = biometrics.getJSONObject(ServerInterface.EMERGENCY.OXYGENSATURATION).getBoolean(ServerInterface.EMERGENCY.ALARM);
            }catch(Exception e){
                _oxygenSaturation=-1;
                _oxygenSaturation_alarm=false;
            }
            try {
                _heartRate = biometrics.getJSONObject(ServerInterface.EMERGENCY.HEARTRATE).getInt(ServerInterface.EMERGENCY.VALUE);
                _heartRate_alarm = biometrics.getJSONObject(ServerInterface.EMERGENCY.HEARTRATE).getBoolean(ServerInterface.EMERGENCY.ALARM);
            }catch(Exception e){
                _heartRate=-1;
                _heartRate_alarm=false;
            }

        }catch (Exception e){
            Log.d("Json ex",e.getMessage());
        }
    }
    public String getPatientRequest(){
        return _patientRequest;
    }
    public Integer getHeartRate(){
        return _heartRate;
    }
    public Integer getOxygenSaturation(){
        return _oxygenSaturation;
    }
    public Integer getBloodPressure(){
        return _bloodPressure;
    }
    public Integer getTemperature(){
        return _temperature;
    }

    public boolean getHeartRateAlarm(){
        return _heartRate_alarm;
    }
    public boolean getOxygenSaturationAlarm(){
        return _oxygenSaturation_alarm;
    }
    public boolean getBloodPressureAlarm(){
        return _bloodPressure_alarm;
    }
    public boolean getTemperatureAlarm(){
        return _temperature_alarm;
    }
    public int getEmergencyId(){
        return _emergencyId;
    }
    public int getBed(){
        return _bed;
    }
    public int getFloor(){
        return _floor;
    }
    public int getRoom(){
        return _room;
    }
    public String getName(){
        return _patientName;
    }
    public String getPatientId(){
        return _patientId;
    }
    public Integer getEmergencyPriority() {return _emergencyPriority;}
}
