package com.andrei.app.model.request;

import java.util.HashMap;

/**
 * Created by Andrei on 10/2/2015.
 */
public class ConnectionVerifyModel {
    public static HashMap<String,Object> toMap(){
        HashMap<String,Object> map=new HashMap<>();
        HashMap<String,Object> content=new HashMap<>();
        content.put("connection_status",0);
        map.put("packet_type","ping_ack");
        map.put("content",content);
        return map;
    }
}
