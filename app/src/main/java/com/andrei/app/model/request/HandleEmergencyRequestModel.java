package com.andrei.app.model.request;

/**
 * Created by Andrei on 7/31/2015.
 */
import com.andrei.app.ServerInterface;
import com.andrei.app.model.base.BaseRequestModel;

import java.util.HashMap;


public class HandleEmergencyRequestModel extends BaseRequestModel {
    private int nurseId;
    private long emergencyId;
    private String sessionId;
    private boolean confirmation;
    private String nurseMessage;
    public HandleEmergencyRequestModel(int nurseId, long emergencyId,boolean confirmation,String sessionId, String nurseMessage){
        this.nurseId = nurseId;
        this.emergencyId = emergencyId;
        this.sessionId = sessionId;
        this.confirmation=confirmation;
        this.nurseMessage=nurseMessage;
    }

    @Override
    public HashMap<String, Object> toMap() {
        HashMap<String,Object> map = new HashMap<>();
        HashMap<String,Object> content = new HashMap<>();
        content.put(ServerInterface.HANDLE_EMERGENCY.CONFIRMATION,confirmation);
        content.put(ServerInterface.HANDLE_EMERGENCY.EMERGENCYID, emergencyId);
        content.put(ServerInterface.HANDLE_EMERGENCY.NURSEID, nurseId);
        content.put(ServerInterface.BASE_REQUEST.SESSIONID, sessionId);
        content.put(ServerInterface.HANDLE_EMERGENCY.NURSE_MESSAGE,nurseMessage);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.CONTENT,content);
        map.put(ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE,ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE_VALUE_ON_ACK);
        return map;
    }
    public int getNurseId(){
        return nurseId;
    }
    public long getEmergencyId(){
        return emergencyId;
    }
}
