package com.andrei.app.model.request;

/**
 * Created by Andrei on 7/31/2015.
 */

import java.util.HashMap;


import com.andrei.app.ServerInterface;
import com.andrei.app.model.base.BaseRequestModel;

public class EmergenciesRequestModel extends BaseRequestModel {
    private int _departmentId;
    private int _nurseId;
    private String _sessionId;

    public EmergenciesRequestModel(int departmentId,int nurseId,String sessionId){
        _departmentId = departmentId;
        _nurseId = nurseId;
        _sessionId = sessionId;
    }
    public int getNurseId(){
        return _nurseId;
    }
    public int getDepartmentId(){
        return _departmentId;
    }
    public String getSessionId(){
        return _sessionId;
    }
    @Override
    public HashMap<String,Object> toMap(){
        HashMap<String,Object> map = new HashMap<>();
        HashMap<String,Object> content = new HashMap<>();
        content.put(ServerInterface.BASE_REQUEST.SESSIONID,_sessionId);
        content.put(ServerInterface.BASE_REQUEST.NURSEID,_nurseId);
        content.put(ServerInterface.EMERGENCIES_REQUEST.DEPARTMENTID, _departmentId);
        map.put(ServerInterface.EMERGENCIES_REQUEST.PACKET_TYPE,ServerInterface.EMERGENCIES_REQUEST.PACKET_TYPE_VALUE);
        map.put(ServerInterface.EMERGENCIES_REQUEST.CONTENT,content);
        return map;
    }

}
