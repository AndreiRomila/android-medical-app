package com.andrei.app;
/**
 * Created by Andrei on 7/31/2015.
 */

import com.andrei.app.model.entities.EmergencyModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class EmergenciesRepository {

    private class EmergenciesComparatorAscending implements Comparator<EmergencyModel>{

        @Override
        public int compare(EmergencyModel lhs, EmergencyModel rhs) {
            switch (_sortOrder){
                case ORDER_PRIORITY:
                    return lhs.getEmergencyPriority().compareTo(rhs.getEmergencyPriority());
                case ORDER_TEMPERATURE:
                    return lhs.getTemperature().compareTo(rhs.getTemperature());
                case ORDER_HEARTRATE:
                    return lhs.getHeartRate().compareTo(rhs.getHeartRate());
                case ORDER_OXYGENSATURATION:
                    return lhs.getOxygenSaturation().compareTo(rhs.getOxygenSaturation());
                case ORDER_BLOODPRESSURE:
                    return lhs.getBloodPressure().compareTo(rhs.getBloodPressure());
            }
            return 0;
        }
    }
    public static final int ORDER_PRIORITY = 0;
    public static final int ORDER_TEMPERATURE = 1;
    public static final int ORDER_HEARTRATE = 2;
    public static final int ORDER_OXYGENSATURATION = 3;
    public static final int ORDER_BLOODPRESSURE = 4;

    private int _sortOrder;
    private static EmergenciesRepository _instance = new EmergenciesRepository();
    //private HashMap<Integer, EmergencyModel> _models;
    private ArrayList<EmergencyModel> _models;

    protected EmergenciesRepository() {
        _sortOrder = ORDER_PRIORITY;
        _models = new ArrayList<>();
    }

    public void setSortOrder(int sortOrder) {
        _sortOrder = sortOrder;
        Collections.sort(_models,new EmergenciesComparatorAscending());
    }

    public static EmergenciesRepository Instance() {
        return _instance;
    }

    public void addEmergency(EmergencyModel emergencyModel) {
        _models.add(emergencyModel);
    }

    public void clearAll() {
        _models.clear();
    }

    public int getTotalNumber() {
        return _models.size();
    }

    public Object getModel(int position) {
        return _models.get(position);
    }

    public void remove(EmergencyModel model) {
        _models.remove(model);
    }
}
