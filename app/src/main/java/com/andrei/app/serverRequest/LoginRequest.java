package com.andrei.app.serverRequest;

import android.util.JsonReader;

import com.andrei.app.ServerConnection;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.LoginRequestModel;
import com.andrei.app.serverRequest.base.ServerRequestBase;

import org.json.JSONException;
import org.json.JSONObject;
import org.apache.http.client.methods.HttpPost;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class LoginRequest extends ServerRequestBase {
    private LoginRequestModel _model;
    public LoginRequest(LoginRequestModel model) {
        _model = model;
    }
    @Override
    public JSONObject getPostRequest() {
        JSONObject sendToSever = new JSONObject(_model.toMap());
        String serverResponse =null;
        JSONObject response;
        try {
            Socket socket = new Socket(ServerInterface.SERVER_IP, ServerInterface.SERVER_PORT);
            //Socket socket = new Socket("10.0.2.2", 11000);
            ServerConnection.socket=socket;
            PrintWriter out=new PrintWriter((ServerConnection.getSocket().getOutputStream()),true);
            BufferedReader in=new BufferedReader(new InputStreamReader(ServerConnection.getSocket().getInputStream()));
            out.println(sendToSever);
            serverResponse=in.readLine();
        }catch(Exception e){
            return null;
        }
        try{
            response=new JSONObject(serverResponse);
        }catch(Exception e){
            return null;
        }
        return response;
    }

}
