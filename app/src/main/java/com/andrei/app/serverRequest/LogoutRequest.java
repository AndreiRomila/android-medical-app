package com.andrei.app.serverRequest;

import com.andrei.app.ServerConnection;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.LogoutRequestModel;
import com.andrei.app.serverRequest.base.ServerRequestBase;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;


public class LogoutRequest extends ServerRequestBase {
    private LogoutRequestModel _model;
    public LogoutRequest(LogoutRequestModel model){
        _model = model;
    }
    @Override
    public JSONObject getPostRequest() {
        JSONObject sendToSever = new JSONObject(_model.toMap());
        String serverResponse =null;
        JSONObject response;
        try {
            PrintWriter out=new PrintWriter((ServerConnection.getSocket().getOutputStream()),true);
            BufferedReader in=new BufferedReader(new InputStreamReader(ServerConnection.getSocket().getInputStream()));
            out.println(sendToSever);
            serverResponse=in.readLine();
        }catch(Exception e){
            return null;
        }
        try{
            response=new JSONObject(serverResponse);
        }catch(Exception e){
            return null;
        }
        return response;
    }
}
