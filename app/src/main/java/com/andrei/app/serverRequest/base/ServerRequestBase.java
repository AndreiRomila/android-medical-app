package com.andrei.app.serverRequest.base;

import org.apache.http.HttpRequest;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

public abstract class ServerRequestBase {
    public abstract JSONObject getPostRequest();
}
