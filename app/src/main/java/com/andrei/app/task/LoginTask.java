package com.andrei.app.task;
/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.andrei.app.LocalSettings;
import com.andrei.app.activities.LoginActivity;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.LoginRequestModel;
import com.andrei.app.serverRequest.LoginRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class LoginTask extends AsyncTask<Void, Void, JSONObject> {
    private Activity _activity;
    private LoginRequestModel _loginModel;

    public LoginTask(Activity activity, LoginRequestModel model) {
        _activity = activity;
        _loginModel = model;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        LoginRequest request = new LoginRequest(_loginModel);
        return request.getPostRequest();
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if (result != null) {
            try {
                //if (result.getBoolean(ServerInterface.BASE_RESPONSE.HASERROR)) {
                   // ((LoginActivity) _activity).showErrorMessage(result.getString(ServerInterface.BASE_RESPONSE.ERRORMESSAGE));
                 //   return;
                //}
                //-----------------------------------------------------------------

                JSONObject content=result.getJSONObject("content");
                if(content.getInt(ServerInterface.BASE_RESPONSE.ERRORCODE)!=0) {
                    ((LoginActivity) _activity).showErrorMessage(result.getString(ServerInterface.BASE_RESPONSE.HASERROR));
                    return;
                }else
                    if(!content.getBoolean(ServerInterface.BASE_RESPONSE.STATUS)){
                        ((LoginActivity) _activity).showErrorMessage(ServerInterface.BASE_RESPONSE.DENIED);
                        return;
                    }
                //success
                //save data to localSettings
                LocalSettings settings = new LocalSettings(_activity.getApplicationContext());
                settings.saveSettings(result.getJSONObject("settings"));
                //redirect to main view
                ((LoginActivity) _activity).redirectToMainScreen();
            } catch (JSONException e) {
                Log.d("Json error", e.getMessage());
            }
        } else {
            ((LoginActivity) _activity).showErrorMessage("Error while receiving data from server");
        }
    }
}
