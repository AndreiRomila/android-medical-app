package com.andrei.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.andrei.app.EmergenciesListAdapter;
import com.andrei.app.EmergenciesRepository;
import com.andrei.app.ServerConnection;
import com.andrei.app.ServerInterface;
import com.andrei.app.activities.MainActivity;
import com.andrei.app.model.entities.EmergencyModel;
import com.andrei.app.model.request.ConnectionVerifyModel;
import com.andrei.app.model.request.EmergenciesDeleteModel;
import com.andrei.app.model.request.EmergenciesReceivedResponseModel;
import com.andrei.app.model.request.EmergenciesRequestModel;
import com.andrei.app.serverRequest.EmergenciesRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * Created by Andrei on 10/2/2015.
 */
public class GetAllServerMessagesTask extends AsyncTask<Void,Void,Void>{

    private static boolean running;
    private EmergenciesRequestModel model;
    private Activity activity;
    public GetAllServerMessagesTask(){}
    public GetAllServerMessagesTask(Activity activity,EmergenciesRequestModel model,boolean running){
        this.activity=activity;
        this.model=model;
        this.running=running;
    }
    @Override
    protected Void doInBackground(Void... params) {
        EmergenciesRepository emergenciesRepository = EmergenciesRepository.Instance();
        String serverResponse=null;
        JSONObject fromServer;
        JSONObject response;
        JSONObject content;
        try{
            Thread.sleep(10000);
        }catch(Exception e){
        }
        try {
            BufferedReader in=new BufferedReader(new InputStreamReader(ServerConnection.getSocket().getInputStream()));
            PrintWriter out=new PrintWriter((ServerConnection.getSocket().getOutputStream()),true);

            while (running) {
                serverResponse = in.readLine();
                try {
                    fromServer = new JSONObject(serverResponse);
                    if (fromServer.getString(ServerInterface.PING.PACKET_TYPE).equals(ServerInterface.PING.PACKET_TYPE_VALUE)) {
                        response = new JSONObject(ConnectionVerifyModel.toMap());
                        out.println(response);
                    }
                    if(fromServer.getString(ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE).equals("nurse_notification"))
                    {
                        emergenciesRepository.addEmergency(new EmergencyModel(fromServer));
                        try{
                            ((MainActivity) activity).refreshList();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        content=fromServer.getJSONObject(ServerInterface.EMERGENCIES_RESPONSE.CONTENT);
                        EmergenciesReceivedResponseModel emergenciesReceivedResponseModel=new EmergenciesReceivedResponseModel(content.getInt(ServerInterface.EMERGENCIES_RESPONSE.MSG_ID));
                        response=new JSONObject(emergenciesReceivedResponseModel.toMap());
                        out.println(response);
                    }
                    if(fromServer.getString(ServerInterface.EMERGENCIES_RESPONSE.PACKET_TYPE).equals(ServerInterface.EMERGENCIES_RESPONSE.DELETE)){
                        emergenciesRepository.remove(new EmergencyModel(fromServer));
                        try{
                            ((MainActivity) activity).refreshList();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        content=fromServer.getJSONObject(ServerInterface.EMERGENCIES_RESPONSE.CONTENT);
                        EmergenciesDeleteModel emergenciesDeleteModel =new EmergenciesDeleteModel(content.getInt(ServerInterface.EMERGENCIES_RESPONSE.MSG_ID));
                        response=new JSONObject(emergenciesDeleteModel.toMap());
                        out.println(response);
                    }

                    if(fromServer.getString(ServerInterface.LOGOUT_INTERFACE.PACKET_TYPE).equals(ServerInterface.LOGOUT_INTERFACE.LOGOUT_ACK)) {
                        try {
                            if ((fromServer.getJSONObject("content").getInt(ServerInterface.BASE_RESPONSE.ERRORCODE)) == 0) {
                                ((MainActivity) activity).close();
                                return null;
                            }
                        } catch (Exception e) {
                            Log.d("Json error", e.getMessage());
                        }
                        running=false;
                    }
                }catch (JSONException e){
                    ((MainActivity)activity).close();
                };
            }
        } catch(IOException e){
            ((MainActivity)activity).close();
        };
        return null;
    }
    @Override
    protected void onPostExecute(Void result){
        ((MainActivity)activity).close();
    }
    public boolean GetRunning(){ return running; }
    public static void SetRunning(boolean value){running=value;
    }
}
