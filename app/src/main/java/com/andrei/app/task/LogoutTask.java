package com.andrei.app.task;
/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.andrei.app.activities.MainActivity;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.LogoutRequestModel;
import com.andrei.app.serverRequest.LogoutRequest;

import org.json.JSONObject;

public class LogoutTask {
    private Activity _activity;
    private LogoutRequestModel _logoutModel;
    public LogoutTask(Activity activity,LogoutRequestModel model){
        _activity = activity;
        _logoutModel = model;
    }
    public void logout() {
        JSONObject result;
            LogoutRequest request = new LogoutRequest(_logoutModel);
            result= request.getPostRequest();
        if(result != null){
            try{
                if ((result.getJSONObject("content").getInt(ServerInterface.BASE_RESPONSE.ERRORCODE))==0) {
                    (new GetAllServerMessagesTask()).SetRunning(false);
                    ((MainActivity) _activity).close();
                    return;
                }
            }catch (Exception e){
                ((MainActivity) _activity).close();
                Log.d("Json error", e.getMessage());
            }
        }
        else{
            ((MainActivity) _activity).close();
        }
    }
}
