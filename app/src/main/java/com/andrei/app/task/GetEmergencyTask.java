package com.andrei.app.task;
/**
 * Created by Andrei on 7/31/2015.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.andrei.app.EmergenciesRepository;
import com.andrei.app.activities.MainActivity;
import com.andrei.app.ServerInterface;
import com.andrei.app.model.request.EmergenciesRequestModel;
import com.andrei.app.model.entities.EmergencyModel;
import com.andrei.app.serverRequest.EmergenciesRequest;

import org.json.JSONArray;
import org.json.JSONObject;


public class GetEmergencyTask extends AsyncTask<Void,Void,JSONObject> {
    private Activity _activity;
    private EmergenciesRequestModel _requestModel;
    public GetEmergencyTask(Activity activity,EmergenciesRequestModel model){
        _activity = activity;
        _requestModel = model;
    }
    @Override
    protected JSONObject doInBackground(Void... params) {
        EmergenciesRequest request = new EmergenciesRequest(_requestModel);
        return request.getPostRequest();
    }
    @Override
    protected void onPostExecute(JSONObject result){
        if(result != null){
            EmergenciesRepository emergenciesRepository = EmergenciesRepository.Instance();
            emergenciesRepository.clearAll();
            try{
                JSONArray emergenciesObjects = result.getJSONArray(ServerInterface.EMERGENCIES_RESPONSE.EMERGENCIES);
                for(int i = 0;i<emergenciesObjects.length();i++){
                    emergenciesRepository.addEmergency(new EmergencyModel(emergenciesObjects.getJSONObject(i)));
                }
            }catch (Exception e){
                Log.d("Json error", e.getMessage());
            }
            //((MainActivity)_activity).invalidateList();
            ((MainActivity)_activity).refreshList();
        }else{
            ((MainActivity) _activity).close();
        }
    }
}
