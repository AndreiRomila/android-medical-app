package com.andrei.app.task;
/**
 * Created by Andrei on 7/31/2015.
 */

import com.andrei.app.ServerConnection;
import com.andrei.app.model.request.HandleEmergencyRequestModel;

import org.json.JSONObject;

import java.io.PrintWriter;


public class HandleEmergencyTask {
    private HandleEmergencyRequestModel _model;

    public HandleEmergencyTask( HandleEmergencyRequestModel model) {
        _model = model;
    }

    public void response(){
        try {
            JSONObject response = new JSONObject(_model.toMap());
            PrintWriter out = new PrintWriter((ServerConnection.getSocket().getOutputStream()), true);
            out.println(response);
        }catch (Exception e){};
    }
}
